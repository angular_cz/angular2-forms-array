import { Component } from '@angular/core';
import { User } from './model/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  user: User = {
    name: "Pavel",
    surname: "Novák",

    contacts: [{
      phone: '530314975'
    }]

  };


  userSubmit(user: User) {
    console.log('Submitted from reactive form: ', user);
    this.user = user;
  }
}
