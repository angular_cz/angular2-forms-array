export interface Contact {
  phone: string;
}

export interface User {
  name: string;
  surname: string;
  contacts: Contact[];
}
